if ( $(window).width() <= 800) {

  $('.menu-toggle').on('click touchstart', function(e){
    e.preventDefault();

    $('.main-nav').toggleClass('active');
    $('.nav-wrapper').toggleClass('active');
  });

  $('.has-dropdown a').on('click touchstart', function(e){
    if ( $('.dropdown').hasClass('active') ) {
      return true;
    } else {
      e.preventDefault();
      $(this).next('.dropdown').addClass('active');
    }

    $('.back').on('click touchstart', function(e){
        e.preventDefault();

        if ( $(this).parents('.dropdown').hasClass('active') ){
          $('.dropdown').removeClass('active');
        }
    });
  });

    $('.dropdown').prepend('<li><a href="#" class="back">Back</a></li>');

}
